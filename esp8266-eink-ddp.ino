/* ESP8266 Eink DDP (Data Display Pad)
    ShadowTronics 2019 v1.0

    Hardware:
     3D Printed Case * coming soon
     ESP8266 esp-12f
     ESP8266 Breakout Board
     LM1117 3.3V 1A SOT-223 Voltage Regulator
     220uF Compastior * value can be changed
     WaveShare 2.7 in E-Ink Hat * size and configuration can be changed
     5v 1a supply
     Usb to Serial adapter to program the esp-12f the first time to enable OTA updates
     Female to Female dupont jumper wires

    Connections:
    E-INK                USB TO SERIAL ADAPTER
     VCC - 5V            5V - NC unless useing adapter to power the esp12-f
     GND - GND          GND - GND connect all grounds together with supply ground
     DIN - ESP 13        TX - ESP 03
     CLK - ESP 14        RX - ESP 01
     CS  - ESP 02       GND - ESP 00  This is a temp ground to enter programing mode of the esp-12
     DC  - ESP 04
     RST - ESP 05
    BUSY - ESP 16

   Libarays: All will be in the folder but also can be installed in the Library Manager

    ESP8266:                GxEPD2:                   Adafruit GFX

      ESP8266WiFi            GxEPD2_BW                 Adafruit_GFX
      WiFiClient             GxEPD2_3C                 gfxfont
      ESP8266WebServer       Fonts/FreeMonoBold9pt7b
      ESP8266mDNS
      ESP8266HTTPUpdateServer

      Protocalls:
      First Protocall will be an RSS reader fetching the Bible verse of the day from
      www.biblegateway.com Once we have got that working We will move to the second protocall
      which will be MQTT to fetch information from our home automation Server running home-assistant.io

*/


#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>
#include <GxEPD2.h>
#include <GxEPD2_BW.h>
#include <GxEPD2_EPD.h>
#include <GxEPD2_GFX.h>
//#include <Fonts/FreeMonoBold9pt7b.h>
#include "RSSReader.h"
#include "ja7.h"
#include "ja9.h"
String titleString1;
String titleString2;
String contentString1;
int loops = 0;
int titlecalled = 0;
int testdel = 0;
int len = 0 ;
int lookup = 0;
// Array of feed URLs
const char *rssFeedURLs [] = {
  "http://www.biblegateway.com/votd/get/?format=atom&version=NASB",
  //"http://rss.cnn.com/rss/cnn_topstories.rss",
  //"http://feeds.bbci.co.uk/news/rss.xml",
  //"http://hosted.ap.org/lineups/SCIENCEHEADS-rss_2.0.xml?SITE=OHLIM&SECTION=HOME",
  //"http://www.latimes.com/rss2.0.xml",
  //"http://rss.cnn.com/rss/cnn_tech.rss",
  //"http://feeds.reuters.com/reuters/topNews",
  //"rssfeeds.usatoday.com/usatoday-NewsTopStories",
};

#define NUMBER_OF_FEEDS (sizeof(rssFeedURLs) / sizeof(char *))
int rssFeedIndex;
GxEPD2_BW<GxEPD2_270, GxEPD2_270::HEIGHT> display(GxEPD2_270(/*CS=2*/ 2, /*DC=4*/ 4, /*RST=5*/ 5, /*BUSY=16*/ 16));

// Create RSS reader instance with 3 second timeout
RSSReader reader = RSSReader(3000);

// Callback called every time a title tag is found in the RSS XML
void titleCallback(char *titleStr) {
  if (titlecalled == true) {
    Serial.print("\nPassage: ");
    Serial.println(titleStr);
    String titleString(titleStr);
    titleString2 = titleString;
    titlecalled = 0;
  }
  else {
    Serial.print("\nTitle: ");
    Serial.println(titleStr);
    String titleString(titleStr);
    titleString1 = titleString;
    //titleString1.toUpperCase();
    //display.println(titleStr);
    //Serial.println(titleString);
    titlecalled = 1;
  }
}

// Callback called every time a pubDate tag is found in the RSS XML
void pubDateCallback(char *dateStr) {

  Serial.print("PubDate: ");
  Serial.println(dateStr);
}

// Callback called every time a pubDate tag is found in the RSS XML
void contentCallback(char *contentStr) {
  String test(contentStr);
  while (lookup != 1) {
  testdel = test.indexOf(';');
  test.remove(0, (testdel + 1));
  lookup++;
  }
  if (lookup == 2) {
    lookup = 0;
  }
  len = test.length();
  len = len - 9;
  test.remove(len);
  contentString1 = test;
  Serial.print("\nVerse: ");
  Serial.println(test);

}


// WIFI Details
const char* host = "esp8266-webupdate";
const char* ssid = "ShadowTronics";
const char* password = "chiman123sabre";
//String test = "Welcome to Shadowtronics, this is a very long ass string at 4am";
// Display information
//  This constructor is for the Waveshare 2.7 e-paper Hat
//   See the GxEPD2 examples for other displays






// OTA update information //
ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;


// Funcutions

void welcomescreen()
{
  //display.InitDisplay();
  display.setRotation(3);
  display.setFont(&JAi_____7pt7b);
  display.setTextColor(GxEPD_BLACK);
  uint16_t x = (display.width() - 160) / 2;
  uint16_t y = display.height() / 2;
  display.setFullWindow();
  display.firstPage();
  do
  {
    display.fillScreen(GxEPD_WHITE);
    display.setCursor(0, 10);
    //String titleString(*titleStr);
    display.println(titleString1);
    display.setCursor(0, 25);
    display.println(titleString2);
    display.setFont(&JAi_____9pt7b);
    display.setCursor(5, 40);
    display.println(contentString1);

    //display.refresh();
    //Serial.println(titleString);
  }
  while (display.nextPage());
  Serial.println("Welcome Screen");
  //system_deep_sleep(0);
}



void setup(void) {
  Serial.begin(115200);
  Serial.println();
  Serial.println("Booting Sketch...");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  MDNS.begin(host);
  httpUpdater.setup(&httpServer);
  httpServer.begin();
  MDNS.addService("http", "tcp", 80);
  Serial.printf("HTTPUpdateServer ready! Open http://%s.local/update in your browser\n", host);
  delay(2000);
  // Bring the Display online and Show a welcome screen
  display.init(0);
  // first update should be full refresh
  //delay(1000);
  //welcomescreen();

  // Setup callbacks for title and pubDate tags in RSS XML
  reader.setTitleCallback(&titleCallback);
  reader.setPubDateCallback(&pubDateCallback);
  reader.setContentCallback(&contentCallback);

  // Initialize feed index to first entry in feed list
  rssFeedIndex = 0;
}

void loop(void) {
  httpServer.handleClient();  // this must remain at the top of the loop.
  if (loops == 0) {
    // Short delay to keep networking happy
    loops = 1;
    delay(1);

    // Get URL of RSS feed to display
    const char *url = rssFeedURLs[rssFeedIndex];

    // Read and parse the RSS feed
    bool result = reader.read(url);
    //Serial.println(result);
    //display.init(115200);
    welcomescreen();
  }
}
